import { RouterModule, Routes } from '@angular/router';
import { QuienesSomosComponent } from './components/quienes-somos/quienes-somos.component';
import { PropiedadesComponent } from './components/propiedades/propiedades.component';
import { ContactoComponent } from './components/contacto/contacto.component';
const APP_ROUTES: Routes = [
    {path: '', redirectTo: 'Home', pathMatch: 'full'},
    {path: 'Home', component: QuienesSomosComponent},
    {path: 'Propiedades', component: PropiedadesComponent},
    {path: 'Contacto', component: ContactoComponent},
   // {path: 'propiedades/prop/:id', component: HeroeComponent}, FALTA HACER EL DETALLE PROPIEDADES
    {path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
